#!/bin/bash

source env.sh

set -x
set -e

# Create a tarball, in a reproducible way

create_dist_tar_xz () {
	tar \
		--format=v7 \
		--mode='a+rwX,o-w' \
		--owner=0 \
		--group=0 \
		--mtime='@1560859200' \
		-cf dist.tar \
		--files-from=-
	xz dist.tar
}

list_pkg_files () {
	echo manifest
	list_dist
}

for app_id in $STEAM_APP_ID_LIST ; do
	pushd "$app_id"
	list_pkg_files | create_dist_tar_xz
	sha1sum dist.tar.xz
	popd
done
