#!/bin/env bash

# This script runs your build.sh file in the same environment as CI would.

readonly docker_image=$(head -n1 .gitlab-ci.yml | cut -d' ' -f2)

set -x

git submodule update --init --recursive

run_in_docker () {
	docker run --rm --init \
		--hostname="$HOSTNAME" \
		--volume="$HOME:$HOME" \
		--volume=/etc/passwd:/etc/passwd:ro \
		--volume=/tmp:/tmp \
		--env="HOME=$HOME" \
		--user=$UID:$UID \
		--workdir="$PWD" \
		"$docker_image" \
		/dev/init -sg -- /bin/bash -c "$@"
}

#run_in_docker ./build-boost.sh
#run_in_docker ./build-bullet.sh
#run_in_docker ./build-mygui.sh
# warning: there might be silent dependency between ffmpeg and osg!
#run_in_docker ./build-osg.sh
#run_in_docker ./build-ffmpeg.sh
run_in_docker ./build.sh
